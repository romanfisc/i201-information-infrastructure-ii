#! /usr/bin/env python3
print('Content-type: text/html\n')

import cgi, random, MySQLdb
import MySQLdb.cursors as cursors


string = 'i211f16_romafisc'
password = 'my+sql=i211f16_romafisc'

db_con = MySQLdb.connect(host="db.soic.indiana.edu", port = 3306, user=string, passwd=password, db=string, cursorclass=cursors.SSCursor)

cursor = db_con.cursor()

form = cgi.FieldStorage()
robot1 = form.getfirst('robot1', 'Megatron')
robot2 = form.getfirst('robot2', 'Megatron')
robotLst = [form.getfirst('robot1', 'Megatron'), form.getfirst('robot2', 'Megatron')]


try:

    robot1hp = "SELECT hitpoint FROM robot WHERE Name = "
    robot2hp += "'"+robot1+"';"

    cursor.execute(ROB1HP)
    results = cursor.fetchall()
    robot1hitpoints = results[0][0]

except Exception as e:

    print('<p>Something went wrong with the SQL!</p>')
    print(robot1hp, "Error:", e)


try:

    robot2hp = "SELECT hitpoint FROM robot WHERE Name = "
    robot2hp += "'"+robot2+"';"

    cursor.execute(robot2)
    results = cursor.fetchall()
    robot2hitpoints = results[0][0]

except Exception as e:

    print('<p>Something went wrong with the SQL!</p>')
    print(robot2hp, "Error:", e)



while True:
        hitConceder = random.choice(robotLst)

        if robot1 == hitConceder:
            print(robot2 + " hits " + robot1)
            print('<br>')
            robot1hitpoints -= 1

        if robot1 == hitConceder:
            print(robot1 + " hits " + robot2)
            print('<br>')
            robot2hitpoints -= 1

        if robot1hitpoints == 0:
            winner = robot2
            break

        if robot2hitpoints == 0:
            winner = robot1
            break

try:
    SQL = "UPDATE robot SET Wins = Wins+1 WHERE Name = "
    SQL += "'"+winner+"';"


    weapon = "SELECT Weapon FROM robot WHERE Name = "
    weapon += "'"+winner+"';"


    cursor.execute(SQL)
    cursor.execute(weapon)
    results = cursor.fetchall()
    db_con.commit()

except Exception as e:

    print('<p>Something went wrong with the SQL!</p>')
    print(SQL, "Error:", e)
else:
    print('<br>', winner, "wins the round with its", results[0][0]+"!", "<br>Congratulations", winner + '!')

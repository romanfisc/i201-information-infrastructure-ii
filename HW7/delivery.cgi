#Roman Fischer
#121
#! /usr/bin/env python

import cgi

print('Content-type: text/html\n')

html = """
<!doctype html>
<html>
<head><meta charset="utf-8">
<title>Delivery Service</title></head>
    <body>
	<h1>Robot Delivery System Confirmation</h1></br>
	<p>You have selected to have {0} delivered by {1}, {2}</p> </br>
	<p>Your total delivery fee comes to: {3}</p>
    </body>
</html>"""
form = cgi.FieldStorage()
theirItem = form.getfirst('delivery', 'none')
delivery_method = form.getfirst('delivery_method', 'drone')
addons = form.getlist('addons')
if len(addons)==0:
	extras = "with out purchasing other addons"
elif len(addons)==1:
	extras = addons[0]
elif len(addons)==2:
	extras = addons[0]+" and "+addons[1]
elif len(addons)==3:
	extras = addons[0]+", "+addons[1]+", and "+addons[2]

fees = 0

if delivery_method == "drone":
	fees+=10
elif delivery_method == "self driving car":
	fees+=20
else:
	fees+=1000

if "Express Delivery" in addons:
	fees+=30
if "Mostly Not Broken" in addons:
	fees+=20
if "With a Smile" in addons:
	fees+=1



print(html.format(theirItem, delivery_method, extras, fees))

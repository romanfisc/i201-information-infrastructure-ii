###Roman Fischer
###Group 121
##
###define function storm by event
###parse through the data
###Ask user for an event
###get the data required
###Once data is acquired, present in appropriate format.
##import csv
###Number 1- Storm By Event
##data = open('Indiana_Storms.csv','r')  #Open up the data in a readable format
##storm_data = list(csv.reader(data))    #Put the data in a list format to work with it in an easier way.
##
##def storm_by_event(event):                #Define Function
##    for data_entry in storm_data:        #For loop to parse through data
##        if event in data_entry:                #If the event is present, print the data in a presentable way.
##            print('A', event, 'happened on',
##                  data_entry[0][5:6] + '/' + data_entry[1] + '/' + data_entry[0][0:4],'in', data_entry[15],'county')
##
###define function storm by date
###parse through data
###Pull data that matches chosen date
###Print that data out in a presentable format
##import datetime
###Number 2- Storm By Date
##def storm_by_date(date1):         #Define function
##    for data_entry in storm_data: #Parse through data, and if the date is in the data entry print results
##        if date1 == data_entry[0][0:4]+ '/'+ data_entry[0][4:6] + '/' + data_entry[1]: #Use indexing to match up date
##            print('A', data_entry[12], 'happened on', date1, 'in' , data_entry[15], 'county')
##
##
###Number 3
###Ask for date or event
### if event, run storm by event
###if date, run storm by date
###if not valid input, tell them and re ask
##
##print('Please print all dates in the format- YYYY/MM/DD when searching by date.')   #Let user know the format the date must be in
##inp = input('Search by date or by event? (type: "date" or "event"-> ')
##
##while True: #while True used for validiation checking.  If event, then run event function.  if date, run date function.  Otherwise, tell them it's invalid and ask again
##    if inp.lower() == 'event':        #.lower() used in cases such as EvEnt or dATE, etc.
##        event_inp = input('Please enter the type of weather event you are searching for-> ').title()
##        storm_by_event(event_inp)
##        break                                      #End script once data is printed
##    elif inp.lower() == 'date':
##        date_inp = input('Please enter the date you are searching for-> ')
##        storm_by_date(date_inp)
##        break
##    else:
##        print('Not valid. Please try again')
##        inp = input('Search by date or by event? (tpye: "date" or "event"-> ')

#################################################################################################################
        #Number 4# #BONUS#
#################################################################################################################


import csv
#Number 1- Storm By Event
data = open('Indiana_Storms.csv','r')  #Open up the data in a readable format
storm_data = list(csv.reader(data))    #Put the data in a list format to work with it in an easier way.

def storm_by_event(event):                #Define Function
    for data_entry in storm_data:        #For loop to parse through data
        if event in data_entry:                #If the event is present, print the data in a presentable way.
            print('A', event, 'happened on',
                  data_entry[0][5:6] + '/' + data_entry[1] + '/' + data_entry[0][0:4],'in', data_entry[15].title(),'county.', data_entry[48], data_entry[49])
            print('')

#define function storm by date
#parse through data
#Pull data that matches chosen date
#Print that data out in a presentable format
import datetime
#Number 2- Storm By Date
def storm_by_date(date1):         #Define function
    for data_entry in storm_data: #Parse through data, and if the date is in the data entry print results
        if date1 == data_entry[0][0:4]+ '/'+ data_entry[0][4:6] + '/' + data_entry[1]: #Use indexing to match up date
            print(data_entry[48] , 'This was reported in', data_entry[15].title(),'county. ', data_entry[49])
            print('')



print('Please print all dates in the format- YYYY/MM/DD when searching by date.')   #Let user know the format the date must be in
inp = input('Search by date or by event? (type: "date" or "event"-> ')

while True: #while True used for validiation checking.  If event, then run event function.  if date, run date function.  Otherwise, tell them it's invalid and ask again
    if inp.lower() == 'event':        #.lower() used in cases such as EvEnt or dATE, etc.
        event_inp = input('Please enter the type of weather event you are searching for-> ').title()
        storm_by_event(event_inp)
        break                                      #End script once data is printed
    elif inp.lower() == 'date':
        date_inp = input('Please enter the date you are searching for-> ')
        storm_by_date(date_inp)
        break
    else:
        print('Not valid. Please try again')
        inp = input('Search by date or by event? (tpye: "date" or "event"-> ')

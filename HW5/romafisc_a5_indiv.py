
##Homework 5
##Roman Fischer

import urllib.request, csv, datetime

##Parts 1 and 2:
def getQuakeData(orderby, limit):
        url = "http://earthquake.usgs.gov/fdsnws/event/1/query.csv?starttime=2016-09-23%2000:00:00&endtime=2016-09-30%2023:59:59&minmagnitude=4.5&eventtype=earthquake&orderby="
        url+= orderby
        url+= "&limit="
        url+= str(limit)
        web_page = urllib.request.urlopen(url)
        contents = web_page.read().decode(errors = "replace")
        web_page.close()
        new_contents = contents.split("\n")
        return new_contents

##Part 3:
def getTopQuakes(data):

        dic_data = csv.DictReader(data)
        print(("{:16} {:17} {:22} {:15} {:25} {:10}").format("ID", "Mag", "Time", "Lat", "Long", "Place"))
        print("-----" * 25)
        for entry in dic_data:
                print(entry["id"],"\t", entry["mag"],"\t", entry["time"],"\t", entry["latitude"],"\t", entry["longitude"],"\t", entry["place"])



##Part 4:
def getQuakeDetails(quake_id):
        information = csv.DictReader(getQuakeData("magnitude",141))
        print(("{:16} {:17} {:22} {:15} {:25} {:10}").format("ID", "Mag", "Time", "Lat", "Long", "Place"))
        print("-----" * 25)
        for entry in information:
                if entry["id"] == quake_id:
                    print(entry["id"],"\t", entry["mag"],"\t", entry["time"],"\t", entry["latitude"],"\t", entry["longitude"],"\t", entry["place"])
#Part 5:
        user_input = input("Would you like to see this on a map? Y/N: ")
##        if user_input.lower()== "y":
##            for entry in information:
##                    if entry["id"] == quake_id:
##                        latitude1 = entry["latitude"]
##            for entry in information:
##                    if entry["id"] == quake_id:
##                        longitude1 = entry["longitude"]
####### couldnt figure out how to make this work
##            url = "https://maps.googleapis.com/maps/api/staticmap?center=39.1653,"+latitude1+"&zoom=9&markers=39.1653,"+longitude1+"&size=640x640&scale=2&key=AIzaSyADS8f73GSJVtMPMkqCz2bNDoWVrphV5GM"




##main:
##getQuakeData("magnitude",10)
getQuakeDetails("us10006scr")
##getTopQuakes(getQuakeData("magnitude",20))

#Roman Fischer
#Indiv HW 4
import os
import datetime
def pl_translate(word):

    if len(word) <= 1:
        return word+"yay"

    pre = word[0]
    suf = word[1:]

    if pre.upper() == pre:
        cap = True

    else:
        cap = False

    pig = suf + pre.lower() + "ay"

    if cap:
        pig = pig[0].upper() + pig[1:]

    return pig
#Phase 1
def pl_words(user_input):
    user_list = user_input.split(" ")

    translate_list = [pl_translate(word) for word in user_list]
    return translate_list

#Phase 2
##def pl_converter():
##    try:
##        file_name = input("Please input a file you would like to convert: ")
##        output = input("Where would you like your translation written? ")
##        home = os.getcwd()
##        file = os.path.join(home, file_name)
##        file_content = [words for word in open(file, "r") for words in word.split()]
##        string = " ".join(file_content)
##        translated = pl_words(string)
##        output_file = open(output, 'a')
##        output_file.write("\n".join(translated))
##    except:
##        print("Please enter a valid file name")
##        pl_converter()

#Phase 3
##def pl_converter():
##    home = input("Please input a directory you would like to use: ")
##    files = os.listdir(home)
##    lists = []
##    for entry in files:
##        if os.path.isfile(entry):
##            lists.append(entry)
##    print("Current files:\n", lists, "\n")
##    choice = ""
##
##    while choice not in lists:
##        choice = input("Please select a valid file: ")
##    try:
##        file_name = choice
##        name = file_name.split(".")
##        new_name = [name[0], "(pig).",name[1]]
##        output = "".join(new_name)
##        file = os.path.join(home, file_name)
##        file_content = [words for word in open(file, "r") for words in word.split()]
##        string = " ".join(file_content)
##        translated = pl_words(string)
##        path = os.path.join(home,"translations")
##        os.chdir(path)
##        output_file = open(output, 'a')
##        output_file.write(" ".join(translated))
##    except:
##        print("Please enter a valid file name")
##        pl_converter()



#Phase 4
def pl_converter():
    home = input("Please input a directory you would like to use: ")
    files = os.listdir(home)
    lists = []
    for entry in files:
        if os.path.isfile(entry):
            lists.append(entry)
    print("Current files:\n", lists, "\n")
    choice = ""

    while choice not in lists:
        choice = input("Please select a valid file: ")
    try:
        file_name = choice
        name = file_name.split(".")
        new_name = [name[0], "(pig).",name[1]]
        output = "".join(new_name)
        file = os.path.join(home, file_name)
        file_content = [words for word in open(file, "r") for words in word.split()]
        string = " ".join(file_content)
        translated = pl_words(string)
        path = os.path.join(home,"translations")
        os.chdir(path)
        output_file = open(output, 'a')
        output_file.write(" ".join(translated))
        now = datetime.datetime.today()
        print(now.strftime("%A, the %dth of %B, %Y,"),now.time())
        print("Thank you for using the Pig Latin Translator")

    except:
        print("Please enter a valid file name")
        pl_converter()
print(os.getcwd())
pl_converter()

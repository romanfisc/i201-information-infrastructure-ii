import xml.etree.ElementTree as ET
#Roman Fischer
#Part a
def display_book(find):

    root = ET.parse(source="library.xml")

    books = root.findall("book")

    for book in books:

        if find == book.attrib["id"]:
            print("Book Found! \n")
            print("Title:",book.find("title").text,"\n")
            print("Author:",book.find("author").text,"\n")
            print("Price: $"+book.find("price").text,"\n")


##display_book("bk107")
#Part b
##root = ET.parse(source="library.xml")
##
##books = root.findall("book")
##
##for book in books:
##    if book.find("publish_date").text[5:7] == "12" and book.find("genre").text == "Computer":
##        print("Title:",book.find("title").text)
##        print("Author:",book.find("author").text)
##        print("Price: $"+book.find("price").text,"\n")


#part c
root = ET.parse(source="library.xml")

books = root.findall("book")

genres = []

for book in books:
    if book.find("genre").text not in genres:
        genres.append(book.find("genre").text)
print("The Genres are:")
print("-"*25)
for genre in genres:
    print(genre)
